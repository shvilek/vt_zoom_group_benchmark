QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network printsupport

TARGET = zoom_grp_test
#CONFIG += console
#CONFIG -= app_bundle
CONFIG += c++11

LIBS += -lprotobuf
LIBS += -licuuc
LIBS += -lmapnik
#LIBS += -lgdal
LIBS += -lproj
LIBS += -lz

TEMPLATE = app
INCLUDEPATH += ./deps/agg/include
INCLUDEPATH += ./deps/protozero/include
INCLUDEPATH += ./deps/clipper/cpp/
INCLUDEPATH += ./../mapnik-vector-tile/src
INCLUDEPATH += /usr/local/include/mapnik/

SOURCES += main.cpp \
    task.cpp \
    vector_tile.pb.cc \
    downloadmanager.cpp \
    qcustomplot.cpp

HEADERS += \
    task.h \
    vector_tile.pb.h \
    catch.hpp \
    downloadmanager.h \
    qcustomplot.h


