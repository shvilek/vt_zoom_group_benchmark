#ifndef TASK_H
#define TASK_H

#include <QObject>
#include <QApplication>
#include <iostream>
#include "downloadmanager.h"
#include <QElapsedTimer>
#include <map>
#include <vector>
#include "qcustomplot.h"

class Task : public QObject
{
    Q_OBJECT
public:
    typedef std::map<int, std::pair<QVector<double>, QVector<double> > > time_result_t; // key=zoom; pair = x,y
    enum CommandType {
        SEG_END    = 0,
        SEG_MOVETO = 1,
        SEG_LINETO = 2,
        SEG_CLOSE = (0x40 | 0x0f)
    };

    enum eGeomType {
        Unknown = 0,
        Point = 1,
        LineString = 2,
        Polygon = 3
    };

    Task(QApplication *parent = 0);

public slots:
    int render_test(QString aFileName);

signals:
    void finished();

protected:
    bool use_meta;
    int map_zoom ;
    std::string pbf_server;
    std::string pbf_dir;
    int tile_x;
    int tile_y;
    int map_w;
    int map_h;
    int depth;
    DownloadManager dwnl_mgr;
    QElapsedTimer time_line;
    QElapsedTimer time_render;
    time_result_t time_result1;
    time_result_t time_result2;
    QCustomPlot* plot;
};

#endif // TASK_H
