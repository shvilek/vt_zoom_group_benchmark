#include "task.h"
#include "vector_tile.pb.h"

#include <vector_tile_compression.hpp>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <map>

#include <mapnik/util/fs.hpp>
#include <mapnik/layer.hpp>
#include <mapnik/map.hpp>
#include <mapnik/agg_renderer.hpp>
#include <mapnik/load_map.hpp>
#include <mapnik/image_util.hpp>
#include <mapnik/geometry.hpp>
#include <mapnik/datasource_cache.hpp>
#include <mapnik/scale_denominator.hpp>
#include <mapnik/projection.hpp>

// vector output api
#include <vector_tile_compression.hpp>
//#include <vector_tile_processor.hpp>
#include <vector_tile_strategy.hpp>
#include <vector_tile_backend_pbf.hpp>
#include <vector_tile_util.hpp>
#include <vector_tile_projection.hpp>
#include <vector_tile_geometry_decoder.hpp>
#include <vector_tile_datasource.hpp>
#include <vector_tile_datasource_pbf.hpp>
#include <protozero/pbf_reader.hpp>

#include <boost/optional/optional_io.hpp>
#include <boost/static_assert.hpp>
#include <boost/format.hpp>

#include <QDir>
#include <QLabel>
#include <QPixmap>
#include <QDebug>
#include <QFont>

#include "downloadmanager.h"

#include "catch.hpp"

#include <proj_api.h>
#include "qcustomplot.h"
/*
class QPlot : public QCustomPlot
{
    Q_OBJECT

};
*/
class x_pj_free {
    public:
        void operator()(projPJ pj) const {
            if (pj) {
                pj_free(pj);
            }
        }
};

class RenderImpl {
    public:
        std::unique_ptr<void, x_pj_free> pj_source_;
        std::unique_ptr<void, x_pj_free> pj_target_;
    public:
        RenderImpl(mapnik::Map& aMap);
        mapnik::box2d<double> getBbox(int x, int y, int size, int zoom);
        void convertPoint(double &x, double &y, int zoom);
    private:
        mapnik::Map map_;
        bool pj_needTransform_;
};

RenderImpl::RenderImpl(mapnik::Map& aMap) : map_(aMap) {
    pj_needTransform_ = false;
    if(mapnik::MAPNIK_GMERC_PROJ != map_.srs())
    {
        pj_needTransform_ = true;
        pj_source_.reset(pj_init_plus(mapnik::MAPNIK_GMERC_PROJ.c_str()));
        if (!pj_source_) {
            throw std::runtime_error("Invalid source projection");
        }
        pj_target_.reset(pj_init_plus(map_.srs().c_str()));
        if (!pj_target_) {
            throw std::runtime_error("Invalid destination projection");
        }
    }
}
void RenderImpl::convertPoint(double &x, double &y, int zoom) {
    // Web mercator
    static const double bound_x0 = -20037508.3428;
    static const double bound_x1 =  20037508.3428;
    static const double bound_y0 = -20037508.3428;
    static const double bound_y1 =  20037508.3428;

    x = bound_x0 + ((bound_x1 - bound_x0)/ (double)(1<<zoom))* (double)x;
    y = bound_y1 - ((bound_y1 - bound_y0)/ (double)(1<<zoom))* (double)y;

    if(pj_needTransform_)
    {
        auto err = pj_transform(pj_source_.get(), pj_target_.get(), 1, 1, &x, &y, NULL);
        if (err) {
            throw std::runtime_error(
                    (boost::format("pj_transform error %1%: %2%") % err % pj_strerrno(err)).str()
                );
        }
    }
}

mapnik::box2d<double> RenderImpl::getBbox(int x, int y, int size, int zoom) {
    double x1, y1, x2, y2;

    x1 = x;
    y1 = y;
    x2 = x + size;
    y2 = y + size;

    convertPoint(x1, y1, zoom);
    convertPoint(x2, y2, zoom);

    return mapnik::box2d<double>(x1, y1, x2, y2);
}

Task::Task(QApplication *a) : QObject(a)
{
    use_meta = false;
    map_zoom = 9;
    pbf_server = "";
    pbf_dir = "";
    tile_x = 309;
    tile_y = 160;
    map_w = 1;
    map_h = 1;
    depth = 1;
    connect(&dwnl_mgr, SIGNAL(download_complete(QString)), this, SLOT(render_test(QString)));

    for (int arg_i = 1; arg_i < a->arguments().size(); arg_i += 2) {
        qDebug() << a->arguments().at(arg_i);
        qDebug() << a->arguments().at(arg_i + 1);

        if (a->arguments().at(arg_i).compare("/z") == 0 && arg_i + 1 < a->arguments().size()) {
            map_zoom = a->arguments().at(arg_i + 1).toInt();
        }
        else if (a->arguments().at(arg_i).compare("/x") == 0 && arg_i + 1 < a->arguments().size()) {
            tile_x = a->arguments().at(arg_i + 1).toInt();
        }
        else if (a->arguments().at(arg_i).compare("/y") == 0 && arg_i + 1 < a->arguments().size()) {
            tile_y = a->arguments().at(arg_i + 1).toInt();
        }
        else if (a->arguments().at(arg_i).compare("/w") == 0 && arg_i + 1 < a->arguments().size()) {
            map_w = a->arguments().at(arg_i + 1).toInt();
        }
        else if (a->arguments().at(arg_i).compare("/h") == 0 && arg_i + 1 < a->arguments().size()) {
            map_h = a->arguments().at(arg_i + 1).toInt();
        }
        else if (a->arguments().at(arg_i).compare("/pbf_server") == 0 && arg_i + 1 < a->arguments().size()) {
            pbf_server = a->arguments().at(arg_i + 1).toStdString();
        }
        else if (a->arguments().at(arg_i).compare("/pbf_dir") == 0 && arg_i + 1 < a->arguments().size()) {
            pbf_dir = a->arguments().at(arg_i + 1).toStdString();
        }
        else if (a->arguments().at(arg_i).compare("/depth") == 0 && arg_i + 1 < a->arguments().size()) {
            depth = a->arguments().at(arg_i + 1).toInt();
        }
    }

    if (pbf_server.empty() == false) {
        std::stringstream ss;
        ss << pbf_server << "/osm-intl/" << map_zoom << "/" << tile_x << "/" << tile_y <<".pbf";
        dwnl_mgr.doDownload(ss.str().c_str());
    } else if (pbf_dir.empty() == false) {
        QDir dir(pbf_dir.c_str());
        QStringList nameFilter;
        nameFilter << "*.pbf";
        QFileInfoList list = dir.entryInfoList( nameFilter, QDir::Files );
        time_line.start();
        if (list.empty()) return;
        for (int i = 0; i < list.size(); ++i) {
            QString filename = list.at(i).fileName();
            filename = filename.remove(".pbf");
            QStringList file_args = filename.split("_");
            if (file_args.size() == 3) {
                map_zoom = file_args.at(0).toInt();
                if (time_result1.find(map_zoom) == time_result1.end()) {
                    time_result1[map_zoom] = std::make_pair(QVector<double>(), QVector<double>());
                    time_result2[map_zoom] = std::make_pair(QVector<double>(), QVector<double>());
                }
                tile_x = file_args.at(1).toInt();
                tile_y = file_args.at(2).toInt();
                render_test(list.at(i).absoluteFilePath());
            }
        }

        std::vector<QColor> colors;
        colors.push_back(QColor(Qt::red));
        colors.push_back(QColor(Qt::green));
        colors.push_back(QColor(Qt::blue));
        colors.push_back(QColor(Qt::cyan));
        colors.push_back(QColor(Qt::magenta));
        colors.push_back(QColor(Qt::yellow));
        int index = 0;
        for (int i = 0; i < 2; ++i)
            for (time_result_t::const_iterator ib = (i == 0 ? time_result1.begin() : time_result2.begin()); ib != (i == 0 ? time_result1.end() : time_result2.end()); ++ib, ++index) {
                QCustomPlot* plot = new QCustomPlot();
                plot->addGraph();
                std::stringstream ss;
                ss << i << "___zoom_" << ib->first << ".png";
                plot->graph(plot->graphCount() - 1)->setName(ss.str().c_str());

                plot->graph(plot->graphCount() - 1)->setPen(QPen(colors[index%colors.size()]));
                plot->graph(plot->graphCount() - 1)->setLineStyle(QCPGraph::lsLine);
                plot->graph(plot->graphCount() - 1)->setData(ib->second.first, ib->second.second);
                plot->graph(plot->graphCount() - 1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::red, Qt::white, 7));
                QFont legendFont;
                legendFont.setPointSize(9); // and make a bit smaller for legend
                plot->legend->setVisible(true);
                plot->legend->setFont(legendFont);
                plot->legend->setBrush(QBrush(QColor(255,255,255,230)));

                plot->rescaleAxes();
                plot->resize(500, 400);
                plot->savePng(ss.str().c_str(), 1024, 768);
                plot->show();
            }
    }

    qDebug() << a->arguments();
    //emit finished();
}


int pow2(int n) {
  return 1<<n;
}

int Task::render_test(QString aFileName) {
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    std::string filename = "";
    if (pbf_server.empty() == false) {
        filename = "./pbf_data/" + aFileName.toStdString();
    } else if (pbf_dir.empty() == false) {
        filename = aFileName.toStdString();
    } else {
        return -1;
    }

    std::ifstream stream(filename.c_str(),std::ios_base::in|std::ios_base::binary);
    if (!stream.is_open())
    {
        throw std::runtime_error("could not open: '" + filename + "'");
    }

    // we are using lite library, so we copy to a string instead of using ParseFromIstream
    std::string message(std::istreambuf_iterator<char>(stream.rdbuf()),(std::istreambuf_iterator<char>()));
    stream.close();

    mapnik::Map map(256 /*/ pow2(tile_depth)*/, 256 /*/ pow2(tile_depth)*/, "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0.0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs +over");
    vector_tile::Tile tile2;
    if (!tile2.ParseFromString(message))
    {
        std::clog << "failed to parse protobuf\n";
    }

    mapnik::load_map(map,"./project-sputnik.xml");
    map.set_buffer_size(256);
    RenderImpl render_impl(map);

    for (std::size_t j=0; j<static_cast<std::size_t>(map.layer_count()); ++j)
    {
        for (std::size_t i=0;i<static_cast<std::size_t>(tile2.layers_size());++i)
        {
            vector_tile::Tile_Layer const& layer = tile2.layers(i);
            if (layer.name() == map.layers()[j].name()) {
                qDebug() << "found datasource:" << layer.name().c_str();
                std::shared_ptr<mapnik::vector_tile_impl::tile_datasource> ds = std::make_shared<mapnik::vector_tile_impl::tile_datasource>(layer, tile_x, tile_y, map_zoom, map.width());
                map.layers()[j].set_datasource(ds);
            }
        }
    }

    for (int tile_depth = 0; tile_depth <= depth; ++tile_depth)
    {
        int road_zoom_ = tile_depth > 2 ? tile_depth - 2 : tile_depth;

        time_render.start();

        for (int xx = 0; xx < (!tile_depth ? 1 : pow2(tile_depth)); ++xx) {
            for (int yy = 0; yy < (!tile_depth ? 1 : pow2(tile_depth)); ++yy) {
                QElapsedTimer render_small_tile_timer;
                render_small_tile_timer.start();
                mapnik::box2d<double> bbox = render_impl.getBbox(tile_x*(!tile_depth ? 1 : pow2(tile_depth)) + xx, tile_y*(!tile_depth ? 1 :pow2(tile_depth)) + yy, 1, map_zoom + tile_depth);
                mapnik::box2d<double> road_bbox = render_impl.getBbox((tile_x*(!road_zoom_ ? 1 : pow2(road_zoom_))+ xx)*0.25, (tile_y*(!road_zoom_ ? 1 :pow2(road_zoom_))+ yy)*0.25, 1, map_zoom + tile_depth);
                map.zoom_to_box(bbox);

                for (std::size_t j=0; j<static_cast<std::size_t>(map.layer_count()); ++j)
                {
                    if (map.layers()[j].datasource().get() != NULL) {
                        if (map.layers()[j].name() == "highway-labe")
                            static_cast<mapnik::vector_tile_impl::tile_datasource*>(map.layers()[j].datasource().get())->set_envelope(road_bbox);
                        else
                            static_cast<mapnik::vector_tile_impl::tile_datasource*>(map.layers()[j].datasource().get())->set_envelope(bbox);
                    }
                }


                std::stringstream ss;
                QDir dir;

                mapnik::image_rgba8 im(map.width(), map.height());
                mapnik::agg_renderer<mapnik::image_rgba8> ren(map,im);
                ren.apply();
                ss << "png_out/";// << map_zoom + tile_depth;
                dir.mkpath(ss.str().c_str());

                ss << "/tile_" << map_zoom + tile_depth << "_" << tile_x*(!tile_depth ? 1 : pow2(tile_depth)) + xx << "_" << tile_y*(!tile_depth ? 1 : pow2(tile_depth)) + yy << ".png";
                mapnik::save_to_file(im, ss.str().c_str(), "png8");
                qDebug() << "gen complete: " << ss.str().c_str() << " time: "<< render_small_tile_timer.elapsed();
                time_result2[map_zoom + tile_depth].first.push_back(time_line.elapsed());
                time_result2[map_zoom + tile_depth].second.push_back(render_small_tile_timer.elapsed());

            }// for (yy
        }//for (xx
        time_result1[map_zoom + tile_depth].first.push_back(time_line.elapsed());
        time_result1[map_zoom + tile_depth].second.push_back(time_render.elapsed());
    }

    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}

