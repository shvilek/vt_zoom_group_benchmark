// main.cpp

#include <QApplication>
#include <QtCore>
#include <QDebug>

#include "task.h"
#include "downloadmanager.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Task *task = new Task(&a);
    QObject::connect(task, SIGNAL(finished()), &a, SLOT(quit()));

    return a.exec();
}

